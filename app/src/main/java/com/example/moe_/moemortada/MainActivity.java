package com.example.moe_.moemortada;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v7.app.AppCompatActivity;
import android.support.design.widget.NavigationView;

import android.os.Handler;


public class MainActivity extends AppCompatActivity implements
        NavigationView.OnNavigationItemSelectedListener {

    //Declaration
    private static final String NAV_ITEM_ID = "navItemId" ;
    private static final long DRAWER_CLOSE_DELAY_MS = 300 ;
    private DrawerLayout mDrawerLayout;
    private int mNavItemId;
    private ActionBarDrawerToggle mDrawerToggle;
    private final aboutFragment mAboutFragment= new aboutFragment();
    private final projectsFragement mProjectFragment = new projectsFragement();
    private final Handler mDrawerActionHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Initialize Drawer layout -> activity_main
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        //Initialize Toolbar android.support.v7.widget.Toolbar
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        //Load saved navigation state if present
        if (null==savedInstanceState){
            mNavItemId = R.id.drawer_item_1;
        }else{
            mNavItemId = savedInstanceState.getInt(NAV_ITEM_ID);
        }

        // Listen to navigation events
        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation);
        navigationView.setNavigationItemSelectedListener(this);

        //Select the Correct nav menu item
        navigationView.getMenu().findItem(mNavItemId).setChecked(true);

        // Setup the humberger icon to open and close drawer
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar, R.string.open, R.string.close);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        // navigate function to swich between items id
        // parameter item id
        navigate(mNavItemId);
    }

    private void navigate(final int itemId) {
        switch (itemId){
            case R.id.drawer_item_1:
                //start about fragment
                getFragmentManager().beginTransaction().replace(R.id.content,mAboutFragment).commit();
                break;
            case R.id.drawer_item_2:
                //start project fragment
                getFragmentManager().beginTransaction().replace(R.id.content, mProjectFragment).commit();
                break;
        }
    }


    @Override
    public void onConfigurationChanged(final Configuration newConfig){
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }
    @Override
    public void onBackPressed(){
        if(mDrawerLayout.isDrawerOpen(GravityCompat.START)){
            mDrawerLayout.closeDrawer(GravityCompat.START);
        }else{
            super.onBackPressed();
        }
    }
    @Override
    public void onSaveInstanceState(final Bundle outState){
        super.onSaveInstanceState(outState);
        outState.putInt(NAV_ITEM_ID, mNavItemId);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        final int id = item.getItemId();

        if(id== android.support.v7.appcompat.R.id.home){
            return mDrawerToggle.onOptionsItemSelected(item);
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * -Handles clicks on the navigation menu
     * @param menuItem
     * @return
     */
    @Override
    public boolean onNavigationItemSelected(final MenuItem menuItem) {
        //Update highlighted item in the navigation menu
        menuItem.setChecked(true);
        mNavItemId = menuItem.getItemId();

        // Time gap after closing the navigation to see animation
        mDrawerLayout.closeDrawer(GravityCompat.START);
        mDrawerActionHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                navigate(menuItem.getItemId());
            }
        }, DRAWER_CLOSE_DELAY_MS);
        return true;
    }
}
